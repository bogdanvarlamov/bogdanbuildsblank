import jwt
from datetime import datetime, timedelta
from functools import wraps
from flask import request, jsonify
from flask_restful import abort
from jwt import DecodeError, ExpiredSignature
from application import app
from google.appengine.api import oauth
from google.appengine.api.oauth.oauth_api import NotAllowedError
import os


def create(user, access_token):

    payload = {
        'sub': user.key.id(),
        'tpak': access_token,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=14)
    }
    token = jwt.encode(payload, app.config['TOKEN_SECRET'])
    return token.decode('unicode_escape')


def parse(req):
    token = req.headers.get('Authorization').split()[1]
    try:
        decoded = jwt.decode(token, app.config['TOKEN_SECRET'])
        return decoded
    except DecodeError:
        return token


def required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        abort_if_no_auth()
        try:
            payload = parse(request)
#         except DecodeError:
#             response = jsonify(message='Token is invalid')
#             response.status_code = 401
#             return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response
        #g.user_id = payload['sub']
        return f(*args, **kwargs)
    return decorated_function


def required_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'SERVER_SOFTWARE' in os.environ and os.environ['SERVER_SOFTWARE'].startswith('Dev'):
            abort_if_no_auth()
            # NOTE: this depends on the bearer token being the google oauth token
            # and not the custom token we send
            # admin functions require google auth tokens
            try:
                if not oauth.is_current_user_admin(_scope='https://www.googleapis.com/auth/userinfo.email'):
                    abort(403)
            # TODO: make a global exception handler for flask for this?
            except NotAllowedError:
                abort(403)  # TODO: should this be 401 instead?
        return f(*args, **kwargs)
    return decorated_function


def abort_if_no_auth():
    if not request.headers.get('Authorization'):
        abort(401)
