from flask import request, jsonify
from flask_restful import Resource
import os
import httplib2
from oauth2client import client
from apiclient import discovery
from application.models import UserModel

CLIENT_SECRETS = os.path.join(
    os.path.dirname(__file__), 'google_client_secrets.json')


class Google(Resource):

    def post(self):
        flow = client.flow_from_clientsecrets(CLIENT_SECRETS,
                                              # NOTE: subtracting last '/'
                                              redirect_uri=request.url_root[
                                                  :-1],
                                              scope="https://www.googleapis.com/auth/plus.login")
        # it looks like step 1 is always required even though our JS client
        # does it
        auth_url = flow.step1_get_authorize_url()
        credentials = flow.step2_exchange(request.json['code'])
        if credentials.access_token_expired:
            return 401
        else:
            http_auth = credentials.authorize(httplib2.Http())
            plus_service = discovery.build("plus", "v1", http_auth)
            profile = plus_service.people().get(userId="me").execute()
            google_id = profile["id"]
            emailKvp = filter(
                lambda emailKvp: emailKvp["type"] == "account", profile["emails"])[0]
            # TODO: return some sensible error if they don't have an email?
            # will that ever happen?
            email = emailKvp["value"]
            user = UserModel.query(UserModel.google_id == google_id).get()
            if user:
                return jsonify(token=credentials.access_token)
            newuser = UserModel(google_id=google_id, email=email)
            newuser.put()
            return jsonify(token=credentials.access_token)