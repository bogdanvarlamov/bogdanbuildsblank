"""
urls.py

URL dispatch route mappings and error handlers

"""
from flask import render_template

from application import app
from application.api import api, init, status
from application.api.account import Account
from application.api.projects.project import Project
from application.auth.google_oauth import Google

# URL dispatch rules
# App Engine warm up handler
# See
# http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests
app.add_url_rule('/_ah/warmup', 'warmup', view_func=init)

# status check
app.add_url_rule('/api/', 'status', view_func=status)

# api resources
api.add_resource(Account, '/api/account/')
api.add_resource(Project, '/api/project/', '/api/project/<int:id>')

# auth resources
api.add_resource(Google, '/api/auth/google/')

# Error handlers
# Handle 404 errors


@app.errorhandler(404)
def page_not_found(e):
    return "not found", 404

# Handle 500 errors


@app.errorhandler(500)
def server_error(e):
    return "internal error", 500
