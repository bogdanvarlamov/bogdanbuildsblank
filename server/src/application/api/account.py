from flask_restful import Resource
from application.auth import token
from flask import jsonify
from google.appengine.api import oauth


class Account(Resource):

    @token.required
    def get(self):
        is_admin = False
        try:
            is_admin = oauth.is_current_user_admin(
                _scope='https://www.googleapis.com/auth/userinfo.email')
        finally:
            return jsonify(isAdmin=is_admin)
