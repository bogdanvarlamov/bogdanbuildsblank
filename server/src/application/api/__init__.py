from flask import jsonify
from flask_restful import Api
from application import app

api = Api(app)


def status():
    return jsonify({'status': 'running'})


def init():
    return ''
