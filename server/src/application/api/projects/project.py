from flask import Response, request, json
from flask_restful import Resource, abort
from application.auth import token
from application.models import ProjectModel, BitbucketRepoModel


class Project(Resource):

    def get(self, id=None):
        if id:
            project = ProjectModel.get_by_id(id)
            if not project:
                abort(404)
            return Response(json.dumps(project.to_json()),  mimetype='application/json')
        projects = [project.to_json()
                    for project in ProjectModel.query().fetch()]
        return Response(json.dumps(projects),  mimetype='application/json')

    @token.required_admin
    def post(self):
        newproject = ProjectModel()
        newproject.name = request.json['name']
        newproject.description = request.json['description']
        newproject.source_quality_estimate = float(request.json[
            'sourceQualityEstimate'])

        bitbucket_repo = BitbucketRepoModel()
        bitbucket_repo.accountname = request.json['bitbucketAccountname']
        bitbucket_repo.repo_slug = request.json['bitbucketRepoSlug']

        newproject.bitbucket_repo = bitbucket_repo

        if 'isPrivate' in request.json:
            newproject.is_private = request.json['isPrivate']

        newproject.put()
        # TODO: do I care about uniqueness?
        # see https://cloud.google.com/appengine/docs/python/ndb/transactions
        # for ideas on how to make this transactional and uniquely named
        return Response(json.dumps(newproject.to_json()),  mimetype='application/json')
