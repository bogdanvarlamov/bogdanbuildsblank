"""
models.py

App Engine datastore models

break these out into seperate files?

"""
from google.appengine.ext import ndb


class UserModel(ndb.Model):
    email = ndb.StringProperty(required=True)
    facebook_id = ndb.StringProperty()
    google_id = ndb.StringProperty()
    linkedin_id = ndb.StringProperty()
    #github_id = db.Column(db.String(120))
    #twitter_id = db.Column(db.String(120))

    def to_json(self):
        return dict(id=self.key.id(), email=self.email)


class BitbucketRepoModel(ndb.Model):
    accountname = ndb.StringProperty(required=True)
    repo_slug = ndb.StringProperty(required=True)

    def to_json(self):
        return dict(accountname=self.accountname,
                    repoSlug=self.repo_slug)


class ProjectModel(ndb.Model):
    is_private = ndb.BooleanProperty(default=False)
    name = ndb.StringProperty(required=True)
    description = ndb.TextProperty()
    created_date = ndb.DateProperty(auto_now_add=True)
    source_quality_estimate = ndb.FloatProperty(required=True)
    bitbucket_repo = ndb.StructuredProperty(BitbucketRepoModel)

    def to_json(self):
        return dict(id=self.key.id(),
                    name=self.name,
                    description=self.description,
                    createdDate=self.created_date.isoformat(),
                    isPrivate=self.is_private,
                    sourceQualityEstimate=self.source_quality_estimate,
                    bitbucketRepo=self.bitbucket_repo.to_json())
