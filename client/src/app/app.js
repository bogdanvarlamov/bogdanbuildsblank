angular.module( 'bbb', [
  'templates-app',
  'templates-common',
  'bbb.home',
  'bbb.project',
  'bbb.about',
  
  //3rd party
  'ui.router',
  'ngResource',
  'satellizer'
])

.config( function myAppConfig ( 
                                $stateProvider,
                                $urlRouterProvider,
                                $authProvider,
                                $locationProvider
                                 ) {
                                     
  $locationProvider.hashPrefix('!');
  
  $urlRouterProvider.otherwise( '/home' );
  
/*  $authProvider.facebook({
      clientId: '624059410963642'
    });
  
  $authProvider.linkedin({
      clientId: '77cw786yignpzj'
    });*/
  
  $authProvider.httpInterceptor = true; // Add Authorization header to HTTP request
  $authProvider.loginOnSignup = true;
  $authProvider.baseUrl = '/api/'; // API Base URL for the paths below.
  $authProvider.loginRedirect = '/';
  $authProvider.logoutRedirect = '/';
  $authProvider.signupRedirect = '/login';
  $authProvider.loginUrl = '/auth/login';
  $authProvider.signupUrl = '/auth/signup';
  $authProvider.loginRoute = '/login';
  $authProvider.signupRoute = '/signup';
  $authProvider.tokenRoot = false; // set the token parent element if the token is not the JSON root
  $authProvider.tokenName = 'token';
  $authProvider.tokenPrefix = 'satellizer'; // Local Storage name prefix
  $authProvider.unlinkUrl = '/auth/unlink/';
  $authProvider.unlinkMethod = 'get';
  $authProvider.authHeader = 'Authorization';
  $authProvider.authToken = 'Bearer';
  $authProvider.withCredentials = true;
  $authProvider.platform = 'browser'; // or 'mobile'
  $authProvider.storage = 'sessionStorage'; // or 'localStorage'
  
//Google
  $authProvider.google({
	clientId: '637034849461-rtedfj7a1msnpmf7vj815orprcnmqhcq.apps.googleusercontent.com',
    url: '/auth/google/',
    authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
    redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
    scope: ['profile', 'email'],
    scopePrefix: 'openid',
    scopeDelimiter: ' ',
    requiredUrlParams: ['scope'],
    optionalUrlParams: ['display'],
    display: 'popup',
    type: '2.0',
    popupOptions: { width: 580, height: 400 }
  });
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location, $auth, $resource, $window ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      if ( angular.isDefined( toState.data.pageTitle ) ) {
          $scope.pageTitle = toState.data.pageTitle + ' | BogdanBuildsBlank' ;
            }
  });
  
  $scope.isAdmin = function(){
      if($location.absUrl().indexOf("http://localhost:8080") === 0){
          return true;
      }
      var account = $window.sessionStorage.getItem('account');
      var isAdmin = false;
      if(account){
          isAdmin = JSON.parse(account).isAdmin;
      }
      return isAdmin;
  };
  
  $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
          .then(function(result){
              var Account = $resource('/api/account/');
              return Account.get().$promise.then(function(account) {
                  $window.sessionStorage.setItem('account', JSON.stringify(account));
              });
          });
    };
    
  $scope.isAuthenticated = function(){
      return $auth.isAuthenticated();
  };
  
  $scope.logout = function(){
      $window.sessionStorage.removeItem('account');
      $auth.logout();
  };

});

