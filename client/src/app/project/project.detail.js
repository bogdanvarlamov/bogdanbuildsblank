angular.module( 'bbb.project.detail', [
  'ui.router',
  //'placeholders',
  'ui.bootstrap',
  'ngResource',
  'moment.filter',
  'angular-repeat-n'
])

.config(function config( $stateProvider ) {
  $stateProvider
  .state('project', {
      url: '/project/:projectId',
      views: {
          "main": {
            controller: 'ProjectDetailCtrl',
            templateUrl: 'project/project.detail.tpl.html'
          }
      },
      data:{ pageTitle: 'Projects' }
  });
})

.controller( 'ProjectDetailCtrl', function ProjectDetailCtrl( $scope, $stateParams, $resource) {
    $resource('/api/project/:projectId')
        .get({projectId: $stateParams.projectId}).$promise
        .then(function(resultingProj){
            $scope.project = resultingProj;
            $scope.devlog = $resource('https://bitbucket.org/api/1.0/repositories/:accountname/:repoSlug/changesets/?limit=10')
                .get({accountname: resultingProj.bitbucketRepo.accountname, repoSlug: resultingProj.bitbucketRepo.repoSlug});
            
            $scope.sourceQualityFilledStars = resultingProj.sourceQualityEstimate * 10;
        });
            
})

;
