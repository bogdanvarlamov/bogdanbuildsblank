angular.module( 'bbb.project', [
  'ui.router',
  'ui.bootstrap',
  
  'bbb.project.detail'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'projects', {
    url: '/projects',
    views: {
      "main": {
        controller: 'ProjectsCtrl',
        templateUrl: 'project/projects.tpl.html'
      }
    },
    data:{ pageTitle: 'Projects' }
  });
})

.controller( 'ProjectsCtrl', function ProjectsCtrl( $scope, $http, $resource, $location) {
    $scope.isCollapsed = true;
    $scope.toggleCollapsed = function(){
        $scope.isCollapsed = !$scope.isCollapsed;
    };
    
    $scope.newProj = {};
    $scope.addProject = function(){
        
        $http.post('/api/project/',
                $scope.newProj,
                {})
             .then(function(result){
                 $scope.projects.push(result.data);
                 $scope.newProj = {};//reset the item
             });
        
    };
    
    //TODO: remove this. It's temp debug code
    if($location.absUrl().indexOf("http://localhost:8080") === 0){
          $scope.projects = [
            {
                "bitbucketRepo": {"accountname": "bogdanvarlamov", "repoSlug": "bogdanbuildsblank"},
                "createdDate": "2015-09-20",
                "description": "The project for the very website you are looking at right now. How meta is that!?", 
                "id": 1, "isPrivate": false,
                "name": "BogdanBuildsBlank.com",
                "sourceQualityEstimate": 0.29999999999999999
            }];
      }
    else{
        var Project = $resource('/api/project/');
        $scope.projects = Project.query(); 
    }
   
})

;
